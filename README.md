
# ClimaBat

ClimaBat experimental mockup - scaled-down street canyons in real-life climatic conditions

# MOCKUP THERMAL CHARACTERISTICS
## Original design (brown & cool paints)

![photo 2009 cool facade](pictures/2009coolfacades/ClimaBat2.png)
- Ref ARTICLE for ClimaBat design:
Doya, M., Bozonnet, E., & Allard, F. (2012). Experimental measurement of cool facades’ performance in a dense urban environment. Energy and Buildings, 55, 42–50. https://doi.org/10.1016/J.ENBUILD.2011.11.001
- Thermal characteristics (without green wall and green roofs) >> Maxime Doya PhD page 138
  - CONCRETE TANKS - Material Concrete (5 cm thick)
  Thermal conductivity 2.36 (W/m/K) | Density 2150 (kg/ m3) | Thermal capacity 915 (J/ kg/ K)
  - GROUND (from bottom to top - missing specific values)
  natural soil, gravel 40cm, sand 40cm, pressed concrete slabs 3cm (40x40cm², concrete and integrated gravel surface)

## New mockup design (2012 measures) with green wall and white paint :
![photo 2009 cool facade](pictures/2012greenfacade/ClimaBat4.png)
Djedjig, R., Bozonnet, E., & Belarbi, R. (2015). Experimental study of the urban microclimate mitigation potential of green roofs and green walls in street canyons. International Journal of Low-Carbon Technologies, 10(1), 34–44. https://doi.org/10.1093/IJLCT/CTT019


# RADIATIVE SENSORS (university roof, cf. M. Doya PhD Thesis)
- solar radiation phi_S global = diffus+direct (horizontal) - pyranometer 
- pyrgeometer > longwave sky irradiance (emission from the sensor to the sky is subtracted) $$ E_{sky} = \sigma \cdot T_{sky}^4 $$
- pyrgeometer values can be problematic (see Annex 1 in Doya Thesis)

# REFERENCE WEATHER DATA (meteo station)
Weather station Bout-Blanc - la Rochelle (near University)
data >> local hours (UTC+1 winter and UTC+2 summer)
sunrise & sunset >> UTC

# RELATED PUBLICATIONS
- M’Saouri El Bat, Adnane, Zaid Romani, Emmanuel Bozonnet, et Abdeslam Draoui. « Integration of a practical model to assess the local urban interactions in building energy simulation with a street canyon ». Journal of Building Performance Simulation 13, nᵒ 6 (1 novembre 2020): 720‑39. https://doi.org/10.1080/19401493.2020.1818829.
- Manni, Mattia, Emanuele Bonamente, Gabriele Lobaccaro, Francesco Goia, Andrea Nicolini, Emmanuel Bozonnet, et Federico Rossi. « Development and Validation of a Monte Carlo-Based Numerical Model for Solar Analyses in Urban Canyon Configurations ». Building and Environment, 29 décembre 2019, 106638. https://doi.org/10.1016/j.buildenv.2019.106638.
- Djedjig, Rabah, Rafik Belarbi, et Emmanuel Bozonnet. « Experimental study of green walls impacts on buildings in summer and winter under an oceanic climate ». Energy and Buildings 150 (1 septembre 2017): 403‑11. https://doi.org/10.1016/J.ENBUILD.2017.06.032.
- Djedjig, Rabah, Emmanuel Bozonnet, et Rafik Belarbi. « Experimental Study of the Urban Microclimate Mitigation Potential of Green Roofs and Green Walls in Street Canyons ». International Journal of Low-Carbon Technologies 10, nᵒ 1 (mars 2015): 34‑44. https://doi.org/10.1093/IJLCT/CTT019.
- Djedjig, Rabah, Emmanuel Bozonnet, et Rafik Belarbi. « Analysis of thermal effects of vegetated envelopes: Integration of a validated model in a building energy simulation program ». Energy and Buildings 86 (janvier 2015): 93‑103. https://doi.org/10.1016/J.ENBUILD.2014.09.057.
- Doya, Maxime, Emmanuel Bozonnet, et Francis Allard. « Experimental measurement of cool facades’ performance in a dense urban environment ». Energy and Buildings 55 (décembre 2012): 42‑50. https://doi.org/10.1016/J.ENBUILD.2011.11.001.
- Djedjig, Rabah, Rafik Belarbi, et Emmanuel Bozonnet. « Experimental Study of a Green Wall System Effects in Urban Canyon Scene ». In 11th REHVA World Congress & 8th International Conference on IAQVEC, 3086‑94. Prague,Czech Republic, 2013.
- Djedjig, Rabah, Emmanuel Bozonnet, et Rafik Belarbi. « Integration of a green envelope model in a transient building simulation program and experimental comparison ». In Building simulation for a sustainable world, 49‑55. Chambéry, France, 2013. http://www.ibpsa.org/proceedings/BS2013/p_1225.pdf.
- Doya, Maxime, Emmanuel Bozonnet, et Francis Allard. « Investigating changes in façades’ energy balance according to coating optical properties ». In BUILDING SIMULATION 2009, 1753‑60. Glasgow, Scotland, 2009. http://ibpsa.org/proceedings/BS2009/BS09_1753_1760.pdf.
- Kumar, D. E. V. S. Kiran. « Cool Surface as Urban Heat Island Effect Mitigation Strategy for Tropical Climate ». NTU, 2021. https://dr.ntu.edu.sg/handle/10356/153374.
