***_______________________MOCKUP THERMAL CHARACTERISTICS ***

- Original design (brown & cool paints) - Ref ARTICLE for ClimaBat design:
Doya, M., Bozonnet, E., & Allard, F. (2012). Experimental measurement of cool facades� performance in a dense urban environment. Energy and Buildings, 55, 42�50. https://doi.org/10.1016/J.ENBUILD.2011.11.001
- Thermal characteristics (without green wall and green roofs) >> Maxime Doya PhD page 138
CONCRETE TANKS - Material Concrete (5 cm thick)
  Thermal conductivity 2.36 (W/m/K) | Density 2150 (kg/ m3) | Thermal capacity 915 (J/ kg/ K)
GROUND (from bottom to top - missing specific values)
  natural soil, gravel 40cm, sand 40cm, pressed concrete slabs 3cm (40x40cm�, concrete and integrated gravel surface)

- New mockup design (2012 measures) with green wall and white paint :
Djedjig, R., Bozonnet, E., & Belarbi, R. (2015). Experimental study of the urban microclimate mitigation potential of green roofs and green walls in street canyons. International Journal of Low-Carbon Technologies, 10(1), 34�44. https://doi.org/10.1093/IJLCT/CTT019


***_______________________RADIATIVE SENSORS (university roof, cf. M. Doya PhD Thesis) ***
- solar radiation phi_S global = diffus+direct (horizontal) - pyranometer 
- pyrgeometer > longwave sky irradiance (emission from the sensor to the sky is subtracted) E_sky = \sigma * T_sky^4
- pyrgeometer values can be problematic (see Annex 1 in Doya Thesis)


***_______________________REFERENCE WEATHER DATA (meteo station) ***
Weather station Bout-Blanc - la Rochelle (near University)
data >> local hours (UTC+1 winter and UTC+2 summer)
sunrise & sunset >> UTC