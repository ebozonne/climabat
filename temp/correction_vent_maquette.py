import pandas as pd

mesures_canyon = r'ClimaBat_2canyon_07-23-2012 to 08-26-2012.xls'
donnees_canyon = pd.read_excel(mesures_canyon,index_col='TIMESTAMP')   #pas de temps : 5min

#CORRECTION DIRECTION DU VENT
donnees_canyon['WindDir'] = (donnees_canyon['WindDir'] +90 )%360

#ENREGISTREMENT DU NOUVEAU FICHIER
donnees_canyon.to_excel('ClimaBat_2canyon_07-23-2012 to 08-26-2012_corrige.xlsx')

